package com.way.mat.hashpainter.utils;

/**
 * Created by mpodolsky on 10.02.2016.
 */
public interface Constants {

    public static final int TYPE_CLEAR = 0;
    public static final int TYPE_PAINT = 1;
    public static final int TYPE_PAINTED = 2;

}
