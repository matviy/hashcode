package com.way.mat.hashpainter;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.way.mat.hashpainter.utils.Constants;
import com.way.mat.hashpainter.utils.MatrixUtils;
import com.way.mat.hashpainter.utils.RegexParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "painter_tag";

    private int[][] mArray;
    private int[][] mResult = new int [1000000][4];
    private int currentCommand = 0;
    private Point mDimentions;
    public static final boolean showLog = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "start: " + System.currentTimeMillis());
        readData();
//        MatrixUtils.writeMatrix(TAG, mArray);
        new Thread(new Runnable() {
            @Override
            public void run() {
                int[] coords = MatrixUtils.getLongestLine(mArray);
                while (coords != null) {
//            Log.d(TAG, "" + currentCommand);
                    MatrixUtils.writeCoords(coords, mResult, currentCommand);
                    MatrixUtils.paintMatrix(mArray, coords);
                    if (showLog) {
                        MatrixUtils.writeMatrix(TAG, mArray);
                    }
                    coords = MatrixUtils.getLongestLine(mArray);
//                    Log.d(TAG, "PAINT_LINE " + coords[1] + " " + coords[0] + " " + coords[3] + " " + coords[2]);
                    currentCommand++;
                }
                Log.d(TAG, "size: " + currentCommand);
                Log.d(TAG, "end: " + System.currentTimeMillis());
//        String s = MatrixUtils.outputCoords(TAG, mResult, currentCommand);
//        writeFile(s);
            }
        }).start();
    }

    private void readData() {
        try {
            InputStream input = getAssets().open("input/input.in");
            BufferedReader in=
                    new BufferedReader(new InputStreamReader(input, "UTF-8"));
            String str;
            str=in.readLine();
            mDimentions = RegexParser.getDimentions(str);
            mArray = new int[mDimentions.x][mDimentions.y];
            int i = 0;
            while ((str=in.readLine()) != null) {
                putLineIntoArray(str.toCharArray(), i);
                i++;
            }

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFile(String s) {
        try {
            File myFile = new File("/sdcard/mysdfile.txt");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append(s);
            myOutWriter.close();
            fOut.close();
            Toast.makeText(getBaseContext(),
                    "Done writing SD 'mysdfile.txt'",
                    Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void putLineIntoArray(char[] mas, int linePos) {
        for (int i = 0; i < mDimentions.y; i++) {
            mArray[linePos][i] = getType(mas[i]);
        }
    }

    private int getType(char c) {
        if (c == '#') {
            return Constants.TYPE_PAINT;
        } else {
            return Constants.TYPE_CLEAR;
        }
    }

}
